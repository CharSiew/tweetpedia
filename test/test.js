var assert = require('assert');
var request = require('request');
var expect = require('chai').expect;
var should = require('chai').should() //actually call the function



describe('Testing base mocha', function() {
  describe('#indexOf()', function () {
	it('should return -1 when the value is not present', function () {
	  assert.equal(-1, [1,2,3].indexOf(5));
	  assert.equal(-1, [1,2,3].indexOf(0));
	});
  });
});



describe('Routing ', function() {
	var url = 'http://localhost:3000'
	it('Returns 200 for route availability', function(done) {
		request.get(url, function(err, response, body) {
			expect(response.statusCode).equal(200);
			done();
		})
	});
	it('Check bad url returns 404', function(done) {
		request.get(url+"/asdfasdfsa", function(err, response, body) {
			expect(response.statusCode).equal(404);
			done();
		})
	});
	it('getting search url returns 404',function(done){
		request.get(url+"/wikiSearch" ,function(err,res,body){
			expect(res.statusCode).equal(404);
			done();
		})

	})

	it("posting to search url returns result",function(done){
		var urlOptions = {
			url:url+"/wikiSearch",
			form:{searchTerm:"Chicken"}
		}
		request.post(urlOptions,function(err, response, body){
			expect(response.statusCode).equal(200);
			done()
		})
	})
});


describe('Content',function(){
	var url = 'http://localhost:3000'

	//this also checks for twitter authentication
	describe('#Twitter :', function(){
		it("Check if base search term",function(done){
			request.get(url,function(err, response, body){
				expect(response.statusCode).equal(200);
				body.should.include("duck");
				done()

			})
		})

		it("Check posted search term",function(done){
			var urlOptions = {
				url:url+"/",
				form:{query:"chicken"}
			}
			request.post(urlOptions,function(err, response, body){
				expect(response.statusCode).equal(200);
				body.should.include("chicken");
				done();
			});
			
		})
	

	});


	describe('Wikipedia :', function(){
		it("returns potato when queried pototo",function(done){
			var urlOptions = {
				url:url+"/wikiSearch",
				form:{searchTerm:"potato"}
			}
			request.post(urlOptions,function(err, response, body){
				expect(response.statusCode).equal(200);
				body.should.include('potato');
				done();
			});
		});	

		it("query has no article",function(done){
			var urlOptions = {
				url:url+"/wikiSearch",
				form:{searchTerm:"adfasdfasdfasdf"}
			}
			request.post(urlOptions,function(err, response, body){
				expect(response.statusCode).equal(200);
				body.should.include('No search result found');
				done();
			});
		});	

		it("query is not in english",function(done){
			var urlOptions = {
				url:url+"/wikiSearch",
				form:{searchTerm:"JAPANに所属するSUGIZOがバイオリン生演奏「ヴェンヌ」がMBFWTで初のランウェイショー開催 "}
			}
			request.post(urlOptions,function(err, response, body){
				expect(response.statusCode).equal(200);
				body.should.include('error');
				body.should.include('not in english');
				done();
			});
		});	


	});



});

