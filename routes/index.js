var request = require('request');
var express = require('express');
var OAuth = require('oauth');
var bodyParser = require('body-parser');

var settings = require('../settings/settings')


var router = express.Router();

// these setting will be located in a different location in normal code

var authHeader = {'Authorization':'Bearer '+ settings.twitter.bearerKey}
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false })




/*
* takes oAuth consumer key,secret and sets bearer token in tweetCredentials
*
*/
function authTwitter( callback ,callbackParams){
	var OAuth2 = OAuth.OAuth2;    
	var oauth2 = new OAuth2(settings.twitter.consumerKey,
		settings.twitter.consumerSecret, 
		'https://api.twitter.com/', 
		null,
		'oauth2/token', 
		null);
	oauth2.getOAuthAccessToken(
		'',
		{'grant_type':'client_credentials'},
		function (error, access_token, refresh_token, results){
			if (!error){
				authHeader['Authorization'] = 'Bearer '+access_token;
				if (callback)
					callback.apply(authTwitter,callbackParams);

			}
	
	});

}


/*
* HomePage
*/
router.get('/', function(req, res, next) {
	twitSearcher("Duck",res);
});

/*
* HomePage - post
*/
router.post('/', urlencodedParser ,function(req,res,next){
	query = req.body.query;
	console.log(query);
	twitSearcher(query,res);

});

/*
* searches Twitter given the query and renders response to responseLocation
*/

function twitSearcher(query,responseLocation){
	var options = {
		url: 'https://api.twitter.com/1.1/search/tweets.json?q=' + query,
		headers: authHeader
	}
	request(options,function callback(error, response, body) {
		if (!error && response.statusCode == 200) {
			var info = JSON.parse(body);
			// console.log(info.search_metadata)
			tweets = info.statuses;
			responseLocation.render('newlayout', { title: 'Tweetpedia' , tweets: tweets ,query:query });

		}
		else if(response.statusCode == 401) {
			console.log('401 twitter - unauthorised - authing now');
			authTwitter(twitSearcher,[query,responseLocation]);
	
		}
		else{
			console.log('uncaught status');
			console.log(options);
			console.log(response.statusCode);
		}
	});

}


 


// Gets the first paragraph of the result
router.post('/searching',urlencodedParser,function(req,res,next){
	var query = req.body.searchTerm;

	// check for foreign characters
	var stripped = query.replace(/\W/g, '');
	stripped = query;
	if (stripped.length/query.length > 0.5){

		var options = {
			url: 'https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&explaintext=1&exsectionformat=plain&titles=' + stripped,
		}

		request(options,function callback(error, response, body) {
			if (!error && response.statusCode == 200) {
				var wikiPages = JSON.parse(body);
				var pages = wikiPages.query.pages;
				res.send(pages[Object.keys(pages)[0]]);
			}
			else{
				console.log(response.statusCode);
			}
		});
	}
	else {
		res.send("query probably not in english");
	}
})

// relise a lot on wikis searchresult
router.post('/wikiSearch',urlencodedParser,function(req,res,next){
	var query = req.body.searchTerm;
	var stripped = query.replace(/\W/g, '');
	var responseData = {query:stripped};

	if (stripped.length/query.length > 0.5){
		var queryOptions = {
			url:'https://en.wikipedia.org/w/api.php?action=query&format=json&prop=&list=search&srprop=snippet%7Credirecttitle&srlimit=5&srsearch=' +stripped,
		};

		request(queryOptions,function processSearchResults(error,response,body){
			if(!error && response.statusCode == 200){
				var wikiResponse = JSON.parse(body);
				if (wikiResponse.query.searchinfo.totalhits > 0){
					var firstSearchResult = wikiResponse.query.search[0]; 
					wikiGetSpecificTitle(firstSearchResult.title , responseData,res)
				}
				else {
					responseData["error"] = "No search result found";
					res.send(responseData)
				}

			}
			else {
				console.log(error, response.statusCode);
				responseData["error"] = "search query not complete"
				res.send(responseData)
			}
		});

	}
	else {
		responseData["error"] = "query probably not in english";
		res.send(responseData);
	}

})

/*given a specifc title grabs the associated wikipedia article
*does not check for validity of title
*renders response data to responselocation
*/
function wikiGetSpecificTitle(title,responseData,responseLocation){
	var queryOptions = {
		url: "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&utf8=1&formatversion=1&explaintext=1&titles=" + title,
	};

	request(queryOptions,function processSearchResults(error,response,body){
		if(!error && response.statusCode== 200){
			var wikiResponse = JSON.parse(body);
			var pages = wikiResponse.query.pages;
			var page = pages[Object.keys(pages)[0]]
			var extract = page.extract.split('\n')[0]

			responseData["page"] = {
				title:page.title,
				extract:extract,
			}

			responseLocation.send(responseData);

		}
		else {
			console.log(error, response.statusCode);
			responseData["error"] = "specific query failed"
			responseLocation.send(responseData);
		}
	})
};


router.get('/home',function(req,res,next){
	res.render('index',{ title: 'Tweetpedia'});
});

router.use(function(req, res, next) {
  res.status(404).render('404',{title:'404'});
});

module.exports = router;
